export interface InputCheckStockDto {
  id: string;
}

export interface OutputCheckStockDto {
  id: string;
  stock: number;
}
