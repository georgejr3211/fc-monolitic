import { Id } from "../../../@shared/domain/value-object/id.value-object";
import { Product } from "../../domain/product.entity";
import { ProductGateway } from "../../gateway/product.gateway";
import { InputCheckStockDto, OutputCheckStockDto } from "./check-stock.dto";
import { CheckStockUseCase } from "./check-stock.usecase";

const MockRepository = () => {
  return {
    find: jest.fn().mockReturnValue(
      Promise.resolve(
        new Product({
          id: new Id("1"),
          name: "name",
          description: "description",
          purchasePrice: 1,
          stock: 10,
        })
      )
    ),
    add: jest.fn(),
  };
};

describe("Check Stock UseCase", () => {
  it("should check stock", async () => {
    const productRepository: ProductGateway = MockRepository();
    const checkStockUseCase = new CheckStockUseCase(productRepository);

    const input: InputCheckStockDto = {
      id: "1",
    };

    const output: OutputCheckStockDto = await checkStockUseCase.execute(input);

    expect(output).toEqual({
      id: "1",
      stock: 10,
    });
  });
});
