import { UseCaseInterface } from "../../../@shared/usecase/usecase.interface";
import { ProductGateway } from "../../gateway/product.gateway";
import { InputCheckStockDto, OutputCheckStockDto } from "./check-stock.dto";

export class CheckStockUseCase implements UseCaseInterface {
  constructor(private readonly productGateway: ProductGateway) {}

  async execute(input: InputCheckStockDto): Promise<OutputCheckStockDto> {
    const product = await this.productGateway.find(input.id);

    return {
      id: product.id.value,
      stock: product.stock,
    };
  }
}
