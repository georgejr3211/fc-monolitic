import { ProductGateway } from '../../gateway/product.gateway';
import { InputAddProductDto } from './add-product.dto';
import { AddProductUseCase } from './add-product.usecase';

const MockRepository = (): ProductGateway => {
  return {
    add: jest.fn(),
    find: jest.fn(),
  };
};

describe("Add Product UseCase", () => {
  it("should add a product", async () => {
    const productRepository: ProductGateway = MockRepository();
    const useCase = new AddProductUseCase(productRepository);

    const product: InputAddProductDto = {
      name: "Product 1",
      description: "Product 1 description",
      purchasePrice: 10,
      stock: 10,
    };

    await useCase.execute(product);

    expect(productRepository.add).toHaveBeenCalled();
  });
});
