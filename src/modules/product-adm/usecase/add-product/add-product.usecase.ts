import { Id } from '../../../@shared/domain/value-object/id.value-object';
import { Product } from '../../domain/product.entity';
import { ProductGateway } from '../../gateway/product.gateway';
import { InputAddProductDto, OutputAddProductDto } from './add-product.dto';

export class AddProductUseCase {
  constructor(private readonly productRepository: ProductGateway) {}

  async execute(input: InputAddProductDto): Promise<OutputAddProductDto> {
    const props = {
      id: new Id(),
      name: input.name,
      description: input.description,
      purchasePrice: input.purchasePrice,
      salesPrice: input.salesPrice,
      stock: input.stock,
    };
    const product = new Product(props);
    await this.productRepository.add(product);

    return {
      id: product.id.value,
      name: product.name,
      description: product.description,
      purchasePrice: product.purchasePrice,
      salesPrice: product.salesPrice,
      stock: product.stock,
      createdAt: product.createdAt,
      updatedAt: product.updatedAt,
    };
  }
}
