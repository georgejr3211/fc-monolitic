import { Sequelize } from "sequelize-typescript";

import { ProductAdmFacadeFactory } from "../factory/product-adm.facade.factory";
import { ProductModel } from "../repository/product.model";
import { InputAddProductFacadeDto } from "./product-adm.facade.interface";

describe("ProductAdmFacade Unit Test", () => {
  let sequelize: Sequelize;

  beforeEach(async () => {
    sequelize = new Sequelize({
      dialect: "sqlite",
      storage: ":memory:",
      logging: false,
    });

    sequelize.addModels([ProductModel]);

    await sequelize.sync();
  });

  afterEach(async () => {
    await sequelize.close();
  });

  it("should add product", async () => {
    const productAdmFacade = ProductAdmFacadeFactory.create();

    const input: InputAddProductFacadeDto = {
      name: "Product 1",
      description: "Description 1",
      purchasePrice: 10,
      stock: 10,
    };

    const output = await productAdmFacade.addProduct(input);

    expect(output).toEqual({
      id: expect.any(String),
      name: "Product 1",
      description: "Description 1",
      purchasePrice: 10,
      stock: 10,
      salesPrice: 0,
      createdAt: expect.any(Date),
      updatedAt: expect.any(Date),
    });
  });

  it("should check stock", async () => {
    const productAdmFacade = ProductAdmFacadeFactory.create();

    const input: InputAddProductFacadeDto = {
      name: "Product 1",
      description: "Description 1",
      purchasePrice: 10,
      stock: 10,
    };

    const output = await productAdmFacade.addProduct(input);

    const outputCheckStock = await productAdmFacade.checkStock({
      productId: output.id,
    });

    expect(outputCheckStock).toEqual({
      id: output.id,
      stock: 10,
    });
  });
});
