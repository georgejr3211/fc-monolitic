import { UseCaseInterface } from "../../@shared/usecase/usecase.interface";
import {
  InputAddProductFacadeDto,
  InputCheckStockFacacdeDto,
  OutputAddProductFacadeDto,
  OutputCheckStockFacacdeDto,
  ProductAdmFacadeInterface,
} from "./product-adm.facade.interface";

export interface UseCaseProps {
  addProductUseCase: UseCaseInterface;
  checkStockUseCase: UseCaseInterface;
}

export class ProductAdmFacade implements ProductAdmFacadeInterface {
  private _addProductUseCase: UseCaseInterface;
  private _checkStockUseCase: UseCaseInterface;

  constructor(props: UseCaseProps) {
    this._addProductUseCase = props.addProductUseCase;
    this._checkStockUseCase = props.checkStockUseCase;
  }

  addProduct(
    input: InputAddProductFacadeDto
  ): Promise<OutputAddProductFacadeDto> {
    // caso o dto do caso de uso for diferente do dto da facade, converter o dto da facade para o dto do caso de uso
    return this._addProductUseCase.execute(input);
  }
  checkStock(
    input: InputCheckStockFacacdeDto
  ): Promise<OutputCheckStockFacacdeDto> {
    return this._checkStockUseCase.execute({ id: input.productId });
  }
}
