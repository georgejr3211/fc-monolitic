export interface InputAddProductFacadeDto {
  name: string;
  description: string;
  purchasePrice: number;
  salesPrice?: number;
  stock: number;
}

export interface OutputAddProductFacadeDto {
  id?: string;
  name: string;
  description: string;
  purchasePrice: number;
  salesPrice: number;
  stock: number;
  createdAt: Date;
  updatedAt: Date;
}

export interface InputCheckStockFacacdeDto {
  productId: string;
}

export interface OutputCheckStockFacacdeDto {
  productId: string;
  stock: number;
}

export interface ProductAdmFacadeInterface {
  addProduct(
    input: InputAddProductFacadeDto
  ): Promise<OutputAddProductFacadeDto>;
  checkStock(
    product: InputCheckStockFacacdeDto
  ): Promise<OutputCheckStockFacacdeDto>;
}
