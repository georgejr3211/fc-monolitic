import { UseCaseInterface } from "../../@shared/usecase/usecase.interface";
import { AddProductUseCase } from "../usecase/add-product/add-product.usecase";
import { ProductAdmFacade } from "../facade/product-adm.facade";
import { ProductAdmFacadeInterface } from "../facade/product-adm.facade.interface";
import { ProductGateway } from "../gateway/product.gateway";
import { ProductRepository } from "../repository/product.repository";
import { CheckStockUseCase } from "../usecase/check-stock/check-stock.usecase";

export class ProductAdmFacadeFactory {
  static create(): ProductAdmFacadeInterface {
    const productRepository: ProductGateway = new ProductRepository();

    const addProductUseCase: UseCaseInterface = new AddProductUseCase(
      productRepository
    );

    const checkStockUseCase: UseCaseInterface = new CheckStockUseCase(
      productRepository
    );

    return new ProductAdmFacade({
      addProductUseCase,
      checkStockUseCase,
    });
  }
}
