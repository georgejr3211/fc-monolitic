import { Sequelize } from "sequelize-typescript";

import { Id } from "../../@shared/domain/value-object/id.value-object";
import { Product } from "../domain/product.entity";
import { ProductModel } from "./product.model";
import { ProductRepository } from "./product.repository";

describe("ProductRepository", () => {
  let sequelize: Sequelize;

  beforeEach(async () => {
    sequelize = new Sequelize({
      dialect: "sqlite",
      storage: ":memory:",
      logging: false,
    });

    sequelize.addModels([ProductModel]);

    await sequelize.sync();
  });

  afterEach(async () => {
    await sequelize.close();
  });

  it("should be able to add a product", async () => {
    const productProps = {
      id: new Id("1"),
      name: "Product 1",
      description: "Description 1",
      purchasePrice: 10,
      salesPrice: 20,
      stock: 10,
      createdAt: new Date(),
      updatedAt: new Date(),
    };
    const product = new Product(productProps);
    const productRepository = new ProductRepository();

    await productRepository.add(product);

    const productDb = await ProductModel.findOne({
      where: { id: product.id.value },
    });

    expect(productDb?.id).toBe(product.id.value);
    expect(productDb?.name).toBe(product.name);
    expect(productDb?.description).toBe(product.description);
    expect(productDb?.purchasePrice).toBe(product.purchasePrice);
    expect(productDb?.stock).toBe(product.stock);
  });

  it("should be able to find a product", async () => {
    const productProps = {
      id: new Id("1"),
      name: "Product 1",
      description: "Description 1",
      purchasePrice: 10,
      stock: 10,
    };
    const product = new Product(productProps);
    const productRepository = new ProductRepository();

    await productRepository.add(product);

    const productDb = await productRepository.find(product.id.value);

    expect(productDb?.id.value).toBe(product.id.value);
    expect(productDb?.name).toBe(product.name);
    expect(productDb?.description).toBe(product.description);
    expect(productDb?.purchasePrice).toBe(product.purchasePrice);
    expect(productDb?.stock).toBe(product.stock);
  });
});
