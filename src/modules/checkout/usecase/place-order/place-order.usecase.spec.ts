import { Id } from "../../../@shared/domain/value-object/id.value-object";
import { ClientAdmFacadeInterface } from "../../../client-adm/facade/client-adm.facade.interface";
import { ProductAdmFacadeInterface } from "../../../product-adm/facade/product-adm.facade.interface";
import { StoreCatalogFacadeInterface } from "../../../store-catalog/facade/store-catalog.facade.interface";
import { Product } from "../../domain/product.entity";
import { InputPlaceOrderDto } from "./place-order.dto";
import { PlaceOrderUseCase } from "./place-order.usecase";

const mockClientFacade: ClientAdmFacadeInterface = {
  find: jest.fn().mockResolvedValue(null),
  add: undefined,
};

const mockProductFacade: ProductAdmFacadeInterface = {
  addProduct: undefined,
  checkStock: jest
    .fn()
    .mockImplementation(({ productId }: { productId: string }) =>
      Promise.resolve({ productId, stock: productId === "1" ? 0 : 1 })
    ),
};

const mockCatalogFacade: StoreCatalogFacadeInterface = {
  find: jest.fn().mockResolvedValue(null),
  findAll: jest.fn(),
};

const clientProps = {
  id: "1c",
  name: "Client 0",
  document: "0000",
  email: "client@user.com",
  street: "some address",
  number: 1,
  complement: "",
  city: "some city",
  state: "some state",
  zipCode: "0000",
};

const mockPaymentFacade = {
  process: jest.fn(),
};

const mockCheckoutRepository = {
  addOrder: jest.fn(),
  findOrder: jest.fn(),
};

const mockInvoiceFacade = {
  generate: jest.fn().mockResolvedValue({ id: "1i" }),
  find: jest.fn(),
};

const placeOrderUseCase = new PlaceOrderUseCase(
  mockClientFacade,
  mockProductFacade,
  mockCatalogFacade,
  mockCheckoutRepository,
  mockInvoiceFacade,
  mockPaymentFacade
);

const mockDate = new Date(2000, 1, 1);

describe("Place Order UseCase unit tests", () => {
  describe("validateProducts methods", () => {
    it("should throw an error when no products are selected", async () => {
      const input: InputPlaceOrderDto = {
        clientId: "1",
        products: [],
      };

      await expect(placeOrderUseCase.validateProducts(input)).rejects.toThrow(
        new Error("No products selected")
      );
    });

    it("should throw an error when product is out of stock", async () => {
      let input: InputPlaceOrderDto = {
        clientId: "1",
        products: [{ productId: "1" }],
      };

      await expect(placeOrderUseCase.validateProducts(input)).rejects.toThrow(
        new Error("Product 1 is not available in stock")
      );

      input.products = [{ productId: "0" }, { productId: "1" }];

      await expect(placeOrderUseCase.validateProducts(input)).rejects.toThrow(
        new Error("Product 1 is not available in stock")
      );

      expect(mockProductFacade.checkStock).toHaveBeenCalledTimes(3);
    });
  });

  describe("getProduct method", () => {
    beforeAll(() => {
      jest.useFakeTimers();
      jest.setSystemTime(mockDate);
    });

    afterAll(() => {
      jest.useRealTimers();
    });

    it("should throw an error when product not found", async () => {
      await expect(placeOrderUseCase["getProduct"]("0")).rejects.toThrow(
        new Error("Product not found")
      );
    });

    it("should return a product", async () => {
      mockCatalogFacade.find = jest.fn().mockResolvedValue({
        id: "0",
        name: "Product 1",
        description: "any description",
        salesPrice: 100,
      });

      await expect(placeOrderUseCase["getProduct"]("0")).resolves.toEqual(
        new Product({
          id: new Id("0"),
          name: "Product 1",
          description: "any description",
          salesPrice: 100,
        })
      );
    });
  });

  describe("execute method", () => {
    beforeAll(() => {
      jest.useFakeTimers();
      jest.setSystemTime(mockDate);
    });

    afterAll(() => {
      jest.useRealTimers();
    });
    it("should throw an error when client not found", async () => {
      const input: InputPlaceOrderDto = {
        clientId: "0",
        products: [],
      };

      await expect(placeOrderUseCase.execute(input)).rejects.toThrow(
        new Error("Client not found")
      );
    });

    it("should throw an error when products are not valid", async () => {
      jest
        .spyOn(mockClientFacade, "find")
        .mockReturnValue(Promise.resolve(true as any));

      const mockValidateProducts = jest
        .spyOn(placeOrderUseCase, "validateProducts")
        .mockRejectedValue(new Error("No products selected"));

      const input: InputPlaceOrderDto = {
        clientId: "1",
        products: [],
      };

      await expect(placeOrderUseCase.execute(input)).rejects.toThrow(
        new Error("No products selected")
      );

      expect(mockValidateProducts).toBeCalledTimes(1);
    });

    describe("place an order", () => {
      const mockClientFacade = {
        find: jest.fn().mockResolvedValue(clientProps),
        add: jest.fn(),
      };

      const placeOrderUseCase = new PlaceOrderUseCase(
        mockClientFacade,
        null,
        null,
        mockCheckoutRepository,
        mockInvoiceFacade,
        mockPaymentFacade
      );

      const products: any = {
        "1": new Product({
          id: new Id("1"),
          name: "Product 1",
          description: "any description",
          salesPrice: 100,
        }),
        "2": new Product({
          id: new Id("2"),
          name: "Product 2",
          description: "any description",
          salesPrice: 200,
        }),
      };

      const mockValidateProducts = jest
        .spyOn(placeOrderUseCase, "validateProducts")
        .mockResolvedValue(null);

      const mockGetProduct = jest
        .spyOn(placeOrderUseCase, "getProduct")
        .mockImplementation((productId: keyof typeof products) => {
          return products[productId];
        });

      it("should not be approved", async () => {
        mockPaymentFacade.process = mockPaymentFacade.process.mockReturnValue({
          transactionId: "1t",
          orderId: "1o",
          ammount: 70,
          status: "error",
          createdAt: new Date(),
          updatedAt: new Date(),
        });

        const input: InputPlaceOrderDto = {
          clientId: "1",
          products: [{ productId: "1" }, { productId: "2" }],
        };

        const output = await placeOrderUseCase.execute(input);

        expect(output.invoiceId).toBeNull();
        expect(output.total).toBe(300);
        expect(output.products).toStrictEqual([
          { productId: "1" },
          { productId: "2" },
        ]);
        expect(mockClientFacade.find).toBeCalledWith({ id: "1" });
        expect(mockValidateProducts).toBeCalledWith(input);
        expect(mockGetProduct).toBeCalledTimes(2);
        expect(mockPaymentFacade.process).toHaveBeenCalledWith({
          amount: output.total,
          orderId: output.id,
        });

        expect(mockInvoiceFacade.generate).toHaveBeenCalledTimes(0);
      });

      it("should be approved", async () => {
        mockPaymentFacade.process = mockPaymentFacade.process.mockReturnValue({
          transactionId: "1t",
          orderId: "1o",
          ammount: 300,
          status: "approved",
          createdAt: new Date(),
          updatedAt: new Date(),
        });

        const input: InputPlaceOrderDto = {
          clientId: "1",
          products: [{ productId: "1" }, { productId: "2" }],
        };

        const output = await placeOrderUseCase.execute(input);

        expect(output.invoiceId).toBe("1i");
        expect(output.total).toBe(300);
        expect(output.products).toStrictEqual([
          { productId: "1" },
          { productId: "2" },
        ]);
        expect(mockClientFacade.find).toBeCalledWith({ id: "1" });
        expect(mockPaymentFacade.process).toHaveBeenCalledWith({
          amount: output.total,
          orderId: output.id,
        });

        expect(mockInvoiceFacade.generate).toHaveBeenCalledTimes(1);
      });
    });
  });
});
