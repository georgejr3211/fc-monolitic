import { UseCaseInterface } from "../../@shared/usecase/usecase.interface";
import {
  CheckoutAdmFacadeInterface,
  InputCheckoutFacadeDto,
  OutputCheckoutFacadeDto,
} from "./checkout.facade.interface";

export interface UseCaseProps {
  placeOrderUsecase: UseCaseInterface;
}

export class CheckoutAdmFacade implements CheckoutAdmFacadeInterface {
  _placeOrderUsecase: UseCaseInterface;

  constructor(props: UseCaseProps) {
    this._placeOrderUsecase = props.placeOrderUsecase;
  }

  async checkout(
    input: InputCheckoutFacadeDto
  ): Promise<OutputCheckoutFacadeDto> {
    return await this._placeOrderUsecase.execute(input);
  }
}
