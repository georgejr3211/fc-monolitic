import { Column, ForeignKey, Model, Table } from 'sequelize-typescript';
import { OrderModel } from './order.model';
import { ProductModel } from './product.model';

@Table({
  tableName: 'order_products',
  timestamps: false
})
export class OrderProductModel extends Model {
  @ForeignKey(() => OrderModel)
  @Column
  order_id: string;

  @ForeignKey(() => ProductModel)
  @Column
  product_id: string;
}