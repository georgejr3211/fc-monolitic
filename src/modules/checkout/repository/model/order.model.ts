import { BelongsTo, BelongsToMany, Column, ForeignKey, Model, PrimaryKey, Table } from 'sequelize-typescript';

import { ClientModel } from './client.model';
import { OrderProductModel } from './order_products.model';
import { ProductModel } from './product.model';

@Table({
  tableName: "orders",
  timestamps: false,
})
export class OrderModel extends Model {
  @PrimaryKey
  @Column({ allowNull: false })
  id: string;

  @ForeignKey(() => ClientModel)
  @Column({ allowNull: false })
  client_id: string;

  @BelongsTo(() => ClientModel)
  client: ClientModel;

  @BelongsToMany(() => ProductModel, () => OrderProductModel)
  products: ProductModel[];

  @Column({ allowNull: true, defaultValue: "pending" })
  status: string;
}
