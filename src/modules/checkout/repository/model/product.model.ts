import {
  BelongsToMany,
  Column,
  Model,
  PrimaryKey,
  Table,
} from "sequelize-typescript";

import { OrderModel } from "./order.model";
import { OrderProductModel } from "./order_products.model";

@Table({
  tableName: "products",
  timestamps: false,
})
export class ProductModel extends Model {
  @PrimaryKey
  @Column({ allowNull: false })
  id: string;

  @Column({ allowNull: false })
  name: string;

  @Column({ allowNull: true })
  description: string;

  @Column({ allowNull: false, defaultValue: 0 })
  salesPrice: number;

  @Column({ allowNull: false, defaultValue: 0 })
  purchasePrice: number;

  @Column({ allowNull: false, defaultValue: 0 })
  stock: number;
  
  @Column({ allowNull: false, defaultValue: 0 })
  price: number;

  @BelongsToMany(() => OrderModel, () => OrderProductModel)
  order: OrderModel;
}
