import { Sequelize } from 'sequelize-typescript';

import { Id } from '../../@shared/domain/value-object/id.value-object';
import { Client } from '../domain/client.entity';
import { Order } from '../domain/order.entity';
import { Product } from '../domain/product.entity';
import { CheckoutRepository } from './checkout.repository';
import { ClientModel } from './model/client.model';
import { OrderModel } from './model/order.model';
import { OrderProductModel } from './model/order_products.model';
import { ProductModel } from './model/product.model';

describe("Checkout Repository Unit Tests", () => {
  let sequelize: Sequelize;

  beforeEach(async () => {
    sequelize = new Sequelize({
      dialect: "sqlite",
      storage: ":memory:",
      logging: false,
    });

    sequelize.addModels([
      ProductModel,
      ClientModel,
      OrderModel,
      OrderProductModel,
    ]);

    await sequelize.sync();
  });

  afterEach(async () => {
    await sequelize.close();
  });

  it("should add a new order", async () => {
    const repository = new CheckoutRepository();
    const clientModel = await ClientModel.create({
      id: "1",
      name: "John Doe",
      email: "john@anymail.com",
      document: '12345678901',
      street: "Street",
      city: "City",
      zipCode: "12345",
      number: "123",
      state: "State",
      complement: "Complement",
      createdAt: new Date(),
      updatedAt: new Date(),
    });

    const client = new Client({
      id: new Id(clientModel.id),
      name: clientModel.name,
      email: clientModel.email,
      address: clientModel.street,
    });

    const productModel = await ProductModel.create({
      id: "1",
      name: "Product 1",
      description: "Description 1",
      salesPrice: 10,
    });

    const product = new Product({
      id: new Id(productModel.id),
      name: productModel.name,
      description: productModel.description,
      salesPrice: productModel.salesPrice,
    });

    const order = new Order({
      client: client,
      products: [product],
    });

    const result = await repository.addOrder(order);

    expect(result.id.value).toBe(order.id.value);
    expect(result.client.id.value).toBe(order.client.id.value);
    expect(result.products[0].id.value).toBe(order.products[0].id.value);
  });
});
