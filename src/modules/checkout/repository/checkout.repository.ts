import { Id } from "../../@shared/domain/value-object/id.value-object";
import { Order } from "../domain/order.entity";
import { CheckoutGateway } from "../gateway/checkout.gateway";
import { OrderModel } from "./model/order.model";

export class CheckoutRepository implements CheckoutGateway {
  async addOrder(input: Order): Promise<Order> {
    const orderModel = await OrderModel.create({
      id: input.id.value,
      client_id: input.client.id.value,
      total: input.total,
      status: input.status,
      createdAt: input.createdAt,
      updatedAt: input.updatedAt,
    });

    const order = new Order({
      id: new Id(orderModel.id),
      client: input.client,
      products: input.products,
    });

    return order;
  }
  findOrder(id: string): Promise<Order> {
    throw new Error("Method not implemented.");
  }
}
