import { ClientAdmFacadeFactory } from "../../client-adm/factory/client-adm.facade.factory";
import { InvoiceFacadeFactory } from "../../invoice/factory/invoice.facade.factory";
import { PaymentFacadeFactory } from "../../payment/factory/payment.facade.factory";
import { ProductAdmFacadeFactory } from "../../product-adm/factory/product-adm.facade.factory";
import { StoreCatalogFacadeFactory } from "../../store-catalog/factory/store-catalog.facade.factory";
import { CheckoutAdmFacade } from "../facade/checkout.facade";
import { CheckoutAdmFacadeInterface } from "../facade/checkout.facade.interface";
import { CheckoutRepository } from '../repository/checkout.repository';
import { PlaceOrderUseCase } from "../usecase/place-order/place-order.usecase";

export class CheckoutFacadeFactory {
  static create(): CheckoutAdmFacadeInterface {
    const clientFacade = ClientAdmFacadeFactory.create();
    const productFacade = ProductAdmFacadeFactory.create();
    const storeCatalogFacade = StoreCatalogFacadeFactory.create();
    const invoiceFacade = InvoiceFacadeFactory.create();
    const paymentFacade = PaymentFacadeFactory.create();
    const checkoutRepository = new CheckoutRepository();
    
    const placeOrderUsecase = new PlaceOrderUseCase(
      clientFacade,
      productFacade,
      storeCatalogFacade,
      checkoutRepository,
      invoiceFacade,
      paymentFacade
    );

    return new CheckoutAdmFacade({ placeOrderUsecase });
  }
}
