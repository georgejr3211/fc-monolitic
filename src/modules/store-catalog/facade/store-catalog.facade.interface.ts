export interface InputFindStoreCatalogFacadeDto {
  id: string;
}

export interface OutputFindStoreCatalogFacadeDto {
  id: string;
  name: string;
  description: string;
  salesPrice: number;
}

export interface OutputFindAllStoreCatalogFacadeDto {
  products: {
    id: string;
    name: string;
    description: string;
    salesPrice: number;
  }[];
}

export interface StoreCatalogFacadeInterface {
  find(
    input: InputFindStoreCatalogFacadeDto
  ): Promise<OutputFindStoreCatalogFacadeDto>;

  findAll(): Promise<OutputFindAllStoreCatalogFacadeDto>;
}
