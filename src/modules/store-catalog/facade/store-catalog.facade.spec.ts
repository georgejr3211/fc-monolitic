import { Sequelize } from "sequelize-typescript";
import { StoreCatalogFacadeFactory } from "../factory/store-catalog.facade.factory";
import { ProductModel } from "../repository/product.model";

describe("Store Catalog Facade Unit Test", () => {
  let sequelize: Sequelize;

  beforeEach(async () => {
    sequelize = new Sequelize({
      dialect: "sqlite",
      storage: ":memory:",
      logging: false,
    });

    sequelize.addModels([ProductModel]);

    await sequelize.sync();
  });

  afterEach(async () => {
    await sequelize.close();
  });

  it("should find a product", async () => {
    await ProductModel.create({
      id: "1",
      name: "Product 1",
      description: "Product 1 description",
      salesPrice: 100,
    });

    const storeCatalogfacade = StoreCatalogFacadeFactory.create();

    const output = await storeCatalogfacade.find({ id: "1" });

    expect(output).toEqual({
      id: "1",
      name: "Product 1",
      description: "Product 1 description",
      salesPrice: 100,
    });
  });

  it("should find all products", async () => {
    await ProductModel.create({
      id: "1",
      name: "Product 1",
      description: "Product 1 description",
      salesPrice: 100,
    });

    await ProductModel.create({
      id: "2",
      name: "Product 2",
      description: "Product 2 description",
      salesPrice: 200,
    });

    const storeCatalogfacade = StoreCatalogFacadeFactory.create();

    const output = await storeCatalogfacade.findAll();

    expect(output).toEqual({
      products: [
        {
          id: "1",
          name: "Product 1",
          description: "Product 1 description",
          salesPrice: 100,
        },
        {
          id: "2",
          name: "Product 2",
          description: "Product 2 description",
          salesPrice: 200,
        },
      ],
    });
  });
});
