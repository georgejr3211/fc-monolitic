import { ProductGateway } from "../gateway/product.gateway";
import {
  InputFindStoreCatalogFacadeDto,
  OutputFindAllStoreCatalogFacadeDto,
  OutputFindStoreCatalogFacadeDto,
  StoreCatalogFacadeInterface,
} from "./store-catalog.facade.interface";

export class StoreCatalogFacade implements StoreCatalogFacadeInterface {
  constructor(private productRepository: ProductGateway) {}

  async find(
    input: InputFindStoreCatalogFacadeDto
  ): Promise<OutputFindStoreCatalogFacadeDto> {
    const product = await this.productRepository.find(input.id);

    return {
      id: product.id.value,
      name: product.name,
      description: product.description,
      salesPrice: product.salesPrice,
    };
  }
  async findAll(): Promise<OutputFindAllStoreCatalogFacadeDto> {
    const products = await this.productRepository.findAll();

    return {
      products: products.map((product) => {
        return {
          id: product.id.value,
          name: product.name,
          description: product.description,
          salesPrice: product.salesPrice,
        };
      }),
    };
  }
}
