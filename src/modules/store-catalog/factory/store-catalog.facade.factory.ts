import { StoreCatalogFacade } from "../facade/store-catalog.facade";
import { StoreCatalogFacadeInterface } from "../facade/store-catalog.facade.interface";
import { ProductRepository } from "../repository/product.repository";

export class StoreCatalogFacadeFactory {
  static create(): StoreCatalogFacadeInterface {
    const productRepository = new ProductRepository();
    return new StoreCatalogFacade(productRepository);
  }
}
