import { Id } from "../../../@shared/domain/value-object/id.value-object";
import { Product } from "../../../store-catalog/domain/product.entity";
import { FindAllProductsUseCase } from './find-all-products.usecase';

const product1 = new Product({
  id: new Id("1"),
  name: "Product 1",
  description: "Product 1 description",
  salesPrice: 10,
});

const product2 = new Product({
  id: new Id("2"),
  name: "Product 2",
  description: "Product 2 description",
  salesPrice: 20,
});

const MockRepository = () => {
  return {
    findAll: jest.fn().mockReturnValue(Promise.resolve([product1, product2])),
    find: jest.fn(),
  };
};

describe("Find All Products UseCase Unit Test", () => {
  it("should find all products", async () => {
    const productRepository = MockRepository();
    const useCase = new FindAllProductsUseCase(productRepository);
    const result = await useCase.execute();

    expect(productRepository.findAll).toHaveBeenCalled();
    expect(result.products.length).toBe(2);
    expect(result.products[0].id).toBe("1");
    expect(result.products[0].name).toBe("Product 1");
    expect(result.products[0].description).toBe("Product 1 description");
    expect(result.products[0].salesPrice).toBe(10);
    expect(result.products[1].id).toBe("2");
    expect(result.products[1].name).toBe("Product 2");
    expect(result.products[1].description).toBe("Product 2 description");
    expect(result.products[1].salesPrice).toBe(20);
  });
});
