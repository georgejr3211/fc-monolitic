import { Product } from "../../domain/product.entity";
import { ProductGateway } from "../../gateway/product.gateway";
import { UseCaseInterface } from "./../../../@shared/usecase/usecase.interface";
import { OutputFindAllProductsDto } from "./find-all-products.dto";

export class FindAllProductsUseCase implements UseCaseInterface {
  constructor(private productRepository: ProductGateway) {}

  async execute(): Promise<OutputFindAllProductsDto> {
    const products = await this.productRepository.findAll();
    return {
      products: products.map((product: Product) => ({
        id: product.id.value,
        name: product.name,
        description: product.description,
        salesPrice: product.salesPrice,
        createdAt: product.createdAt,
        updatedAt: product.updatedAt,
      })),
    };
  }
}
