export interface InputFindAllProductsDto {
  id: string;
  name: string;
  description: string;
  salesPrice: number;
}

type FindAllProductsDto = {
  id: string;
  name: string;
  description: string;
  salesPrice: number;
  createdAt: Date;
  updatedAt: Date;
};

export interface OutputFindAllProductsDto {
  products: FindAllProductsDto[];
}
