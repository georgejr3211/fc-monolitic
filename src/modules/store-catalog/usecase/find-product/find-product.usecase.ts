import { UseCaseInterface } from '../../../@shared/usecase/usecase.interface';
import { ProductGateway } from '../../gateway/product.gateway';
import { InputFindProductDto, OutputFindProductDto } from './find-product.dto';

export class FindProductUseCase implements UseCaseInterface {
  constructor(private productRepository: ProductGateway) {}

  async execute(input: InputFindProductDto): Promise<OutputFindProductDto> {
    const product = await this.productRepository.find(input.id);

    return {
      id: product.id.value,
      name: product.name,
      description: product.description,
      salesPrice: product.salesPrice,
      createdAt: product.createdAt,
      updatedAt: product.updatedAt,
    };
  }
}
