import { Sequelize } from "sequelize-typescript";
import { ProductModel } from "./product.model";
import { ProductRepository } from "./product.repository";

describe("Product Repository Unit Test", () => {
  let sequelize: Sequelize;

  beforeEach(async () => {
    sequelize = new Sequelize({
      dialect: "sqlite",
      storage: ":memory:",
      logging: false,
    });

    sequelize.addModels([ProductModel]);

    await sequelize.sync();
  });

  afterEach(async () => {
    await sequelize.close();
  });

  it("should find all products", async () => {
    await ProductModel.create({
      id: "1",
      name: "Product 1",
      description: "Product 1 description",
      salesPrice: 100,
    });

    await ProductModel.create({
      id: "2",
      name: "Product 2",
      description: "Product 2 description",
      salesPrice: 200,
    });

    const productRepository = new ProductRepository();

    const products = await productRepository.findAll();

    expect(products.length).toBe(2);
    expect(products[0].id).toBeDefined();
    expect(products[0].name).toBe("Product 1");
    expect(products[0].description).toBe("Product 1 description");
    expect(products[0].salesPrice).toBe(100);
    expect(products[1].id).toBeDefined();
    expect(products[1].name).toBe("Product 2");
    expect(products[1].description).toBe("Product 2 description");
    expect(products[1].salesPrice).toBe(200);
  });

  it("should find product by id", async () => {
    const product = await ProductModel.create({
      id: "1",
      name: "Product 1",
      description: "Product 1 description",
      salesPrice: 100,
    });

    const productRepository = new ProductRepository();

    const foundProduct = await productRepository.find(product.id);

    expect(foundProduct).toBeDefined();
    expect(foundProduct.id.value).toBe(product.id);
    expect(foundProduct.name).toBe(product.name);
    expect(foundProduct.description).toBe(product.description);
    expect(foundProduct.salesPrice).toBe(product.salesPrice);
  });
});
