import { Column, Model, PrimaryKey, Table } from "sequelize-typescript";

@Table({
  tableName: "products",
  timestamps: false,
})
export class ProductModel extends Model {
  @PrimaryKey
  @Column({ allowNull: false })
  id: string;

  @Column({ allowNull: false })
  name: string;

  @Column({ allowNull: true })
  description: string;

  @Column({ allowNull: false, defaultValue: 0 })
  purchasePrice: number;

  @Column({ allowNull: false, defaultValue: 0 })
  salesPrice: number;

  @Column({ allowNull: false, defaultValue: 0 })
  stock: number;

  @Column({ allowNull: false, defaultValue: 0 })
  price: number;
}
