import { Sequelize } from 'sequelize-typescript';

import { InvoiceFacadeFactory } from '../factory/invoice.facade.factory';
import { InvoiceModel } from '../repository/invoice.model';
import { ProductModel } from '../repository/product.model';

describe("Invoice Facade Unit Test", () => {
  let sequelize: Sequelize;

  beforeEach(async () => {
    sequelize = new Sequelize({
      dialect: "sqlite",
      storage: ":memory:",
      logging: false,
    });

    sequelize.addModels([InvoiceModel, ProductModel]);

    await sequelize.sync();
  });

  afterEach(async () => {
    await sequelize.close();
  });
  it("should create a invoice", async () => {
    const facade = InvoiceFacadeFactory.create();

    const input = {
      name: "John Doe",
      document: "12345678910",
      street: "street",
      city: "city",
      state: "state",
      zipCode: "zipCode",
      number: 1,
      complement: "complement",
      items: [
        {
          id: "1",
          name: "item 1",
          price: 10,
        },
        {
          id: "2",
          name: "item 2",
          price: 20,
        },
      ],
    };

    const output = await facade.generate(input);

    expect(output.id).toBeDefined();
    expect(output.name).toBe(input.name);
    expect(output.document).toBe(input.document);
    expect(output.street).toBe(input.street);
    expect(output.city).toBe(input.city);
    expect(output.state).toBe(input.state);
    expect(output.zipCode).toBe(input.zipCode);
    expect(output.number).toBe(input.number);
    expect(output.complement).toBe(input.complement);
    expect(output.items).toEqual(input.items);
    expect(output.total).toBe(30);
  });

  it("should find an invoice", async () => {
    await InvoiceModel.create(
      {
        id: "1",
        name: "John Doe",
        document: "12345678910",
        street: "street",
        city: "city",
        state: "state",
        zipCode: "zipCode",
        number: 1,
        complement: "complement",
        items: [
          {
            id: "1",
            name: "item 1",
            price: 10,
          },
          {
            id: "2",
            name: "item 2",
            price: 20,
          },
        ],
      },
      { include: ["items"] }
    );

    const facade = InvoiceFacadeFactory.create();

    const input = {
      id: "1",
    };

    const output = await facade.find(input);

    expect(output.id).toBeDefined();
    expect(output.name).toBeDefined();
    expect(output.document).toBeDefined();
    expect(output.address.street).toBeDefined();
    expect(output.address.city).toBeDefined();
    expect(output.address.state).toBeDefined();
    expect(output.address.zipCode).toBeDefined();
    expect(output.address.number).toBeDefined();
    expect(output.address.complement).toBeDefined();
    expect(output.items).toBeDefined();
    expect(output.total).toBeDefined();
  });
});
