import { UseCaseInterface } from "../../@shared/usecase/usecase.interface";
import {
  InputFindInvoiceFacadeDto,
  InputGenerateInvoiceFacadeDto,
  InvoiceFacadeInterface,
  OutputFindInvoiceFacadeDto,
  OutputGenerateInvoiceFacadeDto,
} from "./invoice.facade.dto";

export interface UseCaseProps {
  findInvoiceUsecase: UseCaseInterface;
  generateInvoiceUsecase: UseCaseInterface;
}

export class InvoiceFacade implements InvoiceFacadeInterface {
  findInvoiceUsecase: UseCaseInterface;
  generateInvoiceUsecase: UseCaseInterface;

  constructor(props: UseCaseProps) {
    this.findInvoiceUsecase = props.findInvoiceUsecase;
    this.generateInvoiceUsecase = props.generateInvoiceUsecase;
  }

  async find(
    input: InputFindInvoiceFacadeDto
  ): Promise<OutputFindInvoiceFacadeDto> {
    const output = await this.findInvoiceUsecase.execute(input);
    return output;
  }

  async generate(
    input: InputGenerateInvoiceFacadeDto
  ): Promise<OutputGenerateInvoiceFacadeDto> {
    const output = await this.generateInvoiceUsecase.execute(input);
    return output;
  }
}
