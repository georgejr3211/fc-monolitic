import { InvoiceFacade } from "../facade/invoice.facade";
import { InvoiceFacadeInterface } from "../facade/invoice.facade.dto";
import { InvoiceRepository } from "../repository/invoice.repository";
import { FindInvoiceUseCase } from "../usecase/find-invoice/find-invoice.usecase";
import { GenerateInvoiceUseCase } from "../usecase/generate-invoice/generate-invoice.usecase";

export class InvoiceFacadeFactory {
  static create(): InvoiceFacadeInterface {
    const invoiceRepository = new InvoiceRepository();
    const generateInvoiceUsecase = new GenerateInvoiceUseCase(
      invoiceRepository
    );
    const findInvoiceUsecase = new FindInvoiceUseCase(invoiceRepository);

    return new InvoiceFacade({ generateInvoiceUsecase, findInvoiceUsecase });
  }
}
