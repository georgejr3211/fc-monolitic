import { Id } from "../../@shared/domain/value-object/id.value-object";
import { Invoice } from "../domain/entity/invoice.entity";
import { Product } from "../domain/entity/product.entity";
import { Address } from "../domain/value-object/address.value-object";
import { InvoiceGateway } from "../gateway/invoice.gateway";
import { InvoiceModel } from "./invoice.model";
import { ProductModel } from "./product.model";

export class InvoiceRepository implements InvoiceGateway {
  async find(id: string): Promise<Invoice> {
    const findedInvoice = await InvoiceModel.findByPk(id, {
      include: ["items"],
    });

    const invoice = new Invoice({
      id: new Id(findedInvoice.id),
      name: findedInvoice.name,
      document: findedInvoice.document,
      address: new Address(
        findedInvoice.city,
        findedInvoice.street,
        findedInvoice.number,
        findedInvoice.state,
        findedInvoice.zipCode,
        findedInvoice.complement
      ),
      items: findedInvoice.items.map(
        (item) =>
          new Product({
            id: new Id(item.id),
            name: item.name,
            price: item.price,
          })
      ),
    });

    return invoice;
  }

  async save(invoice: Invoice): Promise<void> {
    try {
      const invoiceResult = await InvoiceModel.create({
        id: invoice.id.value,
        name: invoice.name,
        document: invoice.document,
        street: invoice.address.street,
        number: invoice.address.number,
        complement: invoice.address.complement,
        city: invoice.address.city,
        state: invoice.address.state,
        zipCode: invoice.address.zipCode,
      });

      await Promise.all(
        invoice.items.map((item) =>
          ProductModel.update(
            {
              invoice_id: invoiceResult.id,
              name: item.name,
              price: item.price,
            },
            { where: { id: item.id.value } }
          )
        )
      );
    } catch (error) {
      throw error;
    }
  }
}
