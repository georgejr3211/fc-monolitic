import {
  BelongsTo,
  Column,
  ForeignKey,
  Model,
  PrimaryKey,
  Table,
} from "sequelize-typescript";
import { InvoiceModel } from "./invoice.model";

@Table({
  tableName: "products",
  timestamps: false,
})
export class ProductModel extends Model {
  @PrimaryKey
  @Column({ allowNull: false })
  id: string;

  @Column({ allowNull: false })
  name: string;
  
  @Column({ allowNull: false, defaultValue: 0 })
  price: number;

  @Column({ allowNull: true })
  description: string;

  @Column({ allowNull: false, defaultValue: 0 })
  purchasePrice: number;

  @Column({ allowNull: false, defaultValue: 0 })
  salesPrice: number;

  @Column({ allowNull: false, defaultValue: 0 })
  stock: number;

  @ForeignKey(() => InvoiceModel)
  @Column({ allowNull: true })
  declare invoice_id: string;

  @BelongsTo(() => InvoiceModel)
  invoice: InvoiceModel;
}
