import { Sequelize } from "sequelize-typescript";
import { Id } from "../../@shared/domain/value-object/id.value-object";
import { Invoice } from "../domain/entity/invoice.entity";
import { Product } from "../domain/entity/product.entity";
import { Address } from "../domain/value-object/address.value-object";
import { InvoiceModel } from "./invoice.model";
import { InvoiceRepository } from "./invoice.repository";
import { ProductModel } from "./product.model";

describe("InvoiceRepository", () => {
  let sequelize: Sequelize;

  beforeEach(async () => {
    sequelize = new Sequelize({
      dialect: "sqlite",
      storage: ":memory:",
      logging: false,
    });

    sequelize.addModels([ProductModel, InvoiceModel]);

    await sequelize.sync();
  });

  afterEach(async () => {
    await sequelize.close();
  });

  it("should find a invoice by id", async () => {
    const invoice = await InvoiceModel.create(
      {
        id: "1",
        name: "John Doe",
        document: "12345678910",
        street: "street",
        city: "city",
        state: "state",
        zipCode: "zipCode",
        number: 1,
        complement: "complement",
        items: [
          {
            id: "1",
            name: "item 1",
            price: 10,
          },
          {
            id: "2",
            name: "item 2",
            price: 20,
          },
        ],
      },
      { include: ["items"] }
    );

    const repository = new InvoiceRepository();
    const output = await repository.find(invoice.id);

    expect(output.id.value).toBe(invoice.id);
    expect(output.name).toBe(invoice.name);
    expect(output.document).toBe(invoice.document);
    expect(output.address.street).toBe(invoice.street);
    expect(output.address.city).toBe(invoice.city);
    expect(output.address.state).toBe(invoice.state);
    expect(output.address.zipCode).toBe(invoice.zipCode);
    expect(output.address.number).toBe(invoice.number);
    expect(output.address.complement).toBe(invoice.complement);
    expect(output.items).toHaveLength(2);
    expect(output.getTotal()).toBe(30);
  });

  it("should create a invoice", async () => {
    const repository = new InvoiceRepository();
    const product1 = new Product({
      id: new Id("1"),
      name: "product1",
      price: 10,
    });

    const product2 = new Product({
      id: new Id("2"),
      name: "product2",
      price: 20,
    });
    const invoice = new Invoice({
      id: new Id("1"),
      name: "John Doe",
      document: "12345678910",
      address: new Address(
        "city",
        "street",
        1,
        "state",
        "zipCode",
        "complement"
      ),
      items: [product1, product2],
    });

    await repository.save(invoice);

    const output = await InvoiceModel.findByPk(invoice.id.value, {
      include: ["items"],
    });

    expect(output.id).toBe(invoice.id.value);
    expect(output.name).toBe(invoice.name);
    expect(output.document).toBe(invoice.document);
    expect(output.street).toBe(invoice.address.street);
    expect(output.city).toBe(invoice.address.city);
    expect(output.state).toBe(invoice.address.state);
    expect(output.zipCode).toBe(invoice.address.zipCode);
    expect(output.number).toBe(invoice.address.number);
    expect(output.complement).toBe(invoice.address.complement);
  });
});
