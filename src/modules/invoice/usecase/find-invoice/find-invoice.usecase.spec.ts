import { Address } from "../../domain/value-object/address.value-object";
import { Id } from "../../../@shared/domain/value-object/id.value-object";
import { Invoice } from "../../domain/entity/invoice.entity";
import { Product } from "../../domain/entity/product.entity";
import { FindInvoiceUseCase } from "./find-invoice.usecase";
import { InputFindInvoiceUseCaseDto } from "./find-invoice.dto";

const address = new Address(
  "city",
  "street",
  1,
  "state",
  "zipCode",
  "complement"
);
const product1 = new Product({
  id: new Id("1"),
  name: "product1",
  price: 10,
});

const product2 = new Product({
  id: new Id("2"),
  name: "product2",
  price: 20,
});

const items = [product1, product2];

const invoice = new Invoice({
  id: new Id("1"),
  name: "John Doe",
  document: "12345678910",
  address,
  items,
});

const MockRepository = () => ({
  find: jest.fn().mockReturnValue(Promise.resolve(invoice)),
  save: jest.fn()
});

describe("FindInvoiceUseCase", () => {
  it("should find an invoice", async () => {
    const repository = MockRepository();
    const useCase = new FindInvoiceUseCase(repository);
    const input: InputFindInvoiceUseCaseDto = {
      id: "1",
    };

    const output = await useCase.execute(input);

    expect(output.id).toBe("1");
    expect(output.name).toBe("John Doe");
    expect(output.document).toBe("12345678910");
    expect(output.address.city).toBe("city");
    expect(output.address.street).toBe("street");
    expect(output.address.number).toBe(1);
    expect(output.address.state).toBe("state");
    expect(output.address.zipCode).toBe("zipCode");
    expect(output.address.complement).toBe("complement");
    expect(output.items.length).toBe(2);
    expect(output.items[0].id).toBe("1");
    expect(output.items[0].name).toBe("product1");
    expect(output.items[0].price).toBe(10);
    expect(output.items[1].id).toBe("2");
    expect(output.items[1].name).toBe("product2");
    expect(output.items[1].price).toBe(20);
    expect(output.total).toBe(30);
  });
});
