import { InvoiceGateway } from "../../gateway/invoice.gateway";
import {
  InputGenerateInvoiceUseCaseDto,
  OutputGenerateInvoiceUseCaseDto,
} from "./generate-invoice.dto";
import { GenerateInvoiceUseCase } from "./generate-invoice.usecase";

const MockRepository = (): InvoiceGateway => ({
  find: jest.fn(),
  save: jest.fn(),
});

describe("GenerateInvoiceUseCase", () => {
  it("should generate an invoice", async () => {
    const repository = MockRepository();
    const usecase = new GenerateInvoiceUseCase(repository);
    const input: InputGenerateInvoiceUseCaseDto = {
      name: "John Doe",
      document: "12345678910",
      street: "street",
      city: "city",
      state: "state",
      zipCode: "zipCode",
      number: 1,
      complement: "complement",
      items: [
        {
          id: "1",
          name: "item 1",
          price: 10,
        },
        {
          id: "2",
          name: "item 2",
          price: 20,
        },
      ],
    };

    const output: OutputGenerateInvoiceUseCaseDto = await usecase.execute(
      input
    );

    expect(output.id).toEqual(expect.any(String));
    expect(output.name).toBe(input.name);
    expect(output.document).toBe(input.document);
    expect(output.street).toBe(input.street);
    expect(output.city).toBe(input.city);
    expect(output.state).toBe(input.state);
    expect(output.zipCode).toBe(input.zipCode);
    expect(output.number).toBe(input.number);
    expect(output.complement).toBe(input.complement);
    expect(output.items).toHaveLength(2);
    expect(output.total).toBe(30);
  });
});
