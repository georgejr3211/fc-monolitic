import { Id } from "../../../@shared/domain/value-object/id.value-object";
import { Invoice } from "../../domain/entity/invoice.entity";
import { Product } from "../../domain/entity/product.entity";
import { Address } from "../../domain/value-object/address.value-object";
import { InvoiceGateway } from "../../gateway/invoice.gateway";
import { UseCaseInterface } from "./../../../@shared/usecase/usecase.interface";
import {
  InputGenerateInvoiceUseCaseDto,
  OutputGenerateInvoiceUseCaseDto,
} from "./generate-invoice.dto";

export class GenerateInvoiceUseCase implements UseCaseInterface {
  constructor(private readonly invoiceRepository: InvoiceGateway) {}

  async execute(
    input: InputGenerateInvoiceUseCaseDto
  ): Promise<OutputGenerateInvoiceUseCaseDto> {
    const items = input.items.map(
      (item) =>
        new Product({
          id: new Id(item.id),
          name: item.name,
          price: item.price,
        })
    );

    const address = new Address(
      input.city,
      input.street,
      input.number,
      input.state,
      input.zipCode,
      input.complement
    );

    const invoice = new Invoice({
      name: input.name,
      document: input.document,
      address,
      items,
    });

    await this.invoiceRepository.save(invoice);

    const output: OutputGenerateInvoiceUseCaseDto = {
      id: invoice.id.value,
      name: invoice.name,
      document: invoice.document,
      street: invoice.address.street,
      number: invoice.address.number,
      complement: invoice.address.complement,
      city: invoice.address.city,
      state: invoice.address.state,
      zipCode: invoice.address.zipCode,
      items: invoice.items.map((item) => ({
        id: item.id.value,
        name: item.name,
        price: item.price,
      })),
      total: invoice.getTotal(),
    };

    return output;
  }
}
