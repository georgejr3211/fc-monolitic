import { BaseEntity } from "../../../@shared/domain/entity/base.entity";
import { Id } from "../../../@shared/domain/value-object/id.value-object";

type ProductProps = {
  id: Id;
  name: string;
  price: number;
};

export class Product extends BaseEntity {
  private _name: string;
  private _price: number;

  constructor(props: ProductProps) {
    super(props.id);
    this._name = props.name;
    this._price = props.price;

    this.validate();
  }

  private validate(): void {
    if (!this._name) {
      throw new Error("Name is required");
    }
    if (!this._price) {
      throw new Error("Price is required");
    }
  }

  get name(): string {
    return this._name;
  }

  get price(): number {
    return this._price;
  }
}
