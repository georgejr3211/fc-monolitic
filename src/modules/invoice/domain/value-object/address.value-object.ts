import { ValueObject } from "../../../@shared/domain/value-object/value-object.interface";

export class Address implements ValueObject {
  private _city: string;
  private _street: string;
  private _number: number;
  private _complement?: string;
  private _state: string;
  private _zipCode: string;

  constructor(
    city: string,
    street: string,
    number: number,
    state: string,
    zipCode: string,
    complement?: string
  ) {
    this._city = city;
    this._street = street;
    this._number = number;
    this._complement = complement;
    this._state = state;
    this._zipCode = zipCode;

    this.validate();
  }

  private validate(): void {
    if (!this._city) {
      throw new Error("City is required");
    }
    if (!this._street) {
      throw new Error("Street is required");
    }
    if (!this._number) {
      throw new Error("Number is required");
    }
    if (!this._state) {
      throw new Error("State is required");
    }
    if (!this._zipCode) {
      throw new Error("ZipCode is required");
    }
  }

  get city(): string {
    return this._city;
  }

  get street(): string {
    return this._street;
  }

  get number(): number {
    return this._number;
  }

  get complement(): string | undefined {
    return this._complement;
  }

  get state(): string {
    return this._state;
  }

  get zipCode(): string {
    return this._zipCode;
  }

  toString(): string {
    return `${this.street}, ${this.number} - ${this.state} - ${this.zipCode}`;
  }
}
