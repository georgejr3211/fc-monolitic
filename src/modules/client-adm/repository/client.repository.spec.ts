import { Sequelize } from "sequelize-typescript";

import { Id } from "../../@shared/domain/value-object/id.value-object";
import { Client } from "../domain/client.entity";
import { ClientModel } from "./client.model";
import { ClientRepository } from "./client.repository";

describe("Client Repository Unit Test", () => {
  let sequelize: Sequelize;

  beforeEach(async () => {
    sequelize = new Sequelize({
      dialect: "sqlite",
      storage: ":memory:",
      logging: false,
    });

    sequelize.addModels([ClientModel]);

    await sequelize.sync();
  });

  afterEach(async () => {
    await sequelize.close();
  });

  it("should find a client", async () => {
    const client = await ClientModel.create({
      id: new Id("1").value,
      name: "John Doe",
      email: "x@x.com",
      document: "0000",
      city: "New York",
      state: "NY",
      street: "Some Street",
      number: 1,
      complement: "",
      zipCode: "0000",
      createdAt: new Date(),
      updatedAt: new Date(),
    });

    const repository = new ClientRepository();

    const result = await repository.find(client.id);

    expect(result.id.value).toEqual(client.id);
    expect(result.name).toEqual(client.name);
    expect(result.email).toEqual(client.email);
    expect(result.document).toEqual(client.document);
    expect(result.city).toEqual(client.city);
    expect(result.state).toEqual(client.state);
    expect(result.street).toEqual(client.street);
    expect(result.number).toEqual(client.number);
    expect(result.complement).toEqual(client.complement);
    expect(result.zipCode).toEqual(client.zipCode);
  });

  it("should create a client", async () => {
    const client = new Client({
      id: new Id("1"),
      name: "John Doe",
      email: "x@x.com",
      city: "New York",
      state: "NY",
      street: "Some Street",
      number: 1,
      complement: "",
      document: "0000",
      zipCode: "0000",
    });

    const repository = new ClientRepository();
    await repository.add(client);

    const result = await ClientModel.findByPk(client.id.value);

    expect(result.id).toEqual(client.id.value);
    expect(result.name).toEqual(client.name);
    expect(result.email).toEqual(client.email);
    expect(result.city).toEqual(client.city);
    expect(result.state).toEqual(client.state);
    expect(result.street).toEqual(client.street);
    expect(result.number).toEqual(client.number);
    expect(result.complement).toEqual(client.complement);
    expect(result.document).toEqual(client.document);
    expect(result.zipCode).toEqual(client.zipCode);
    expect(result.createdAt).toStrictEqual(client.createdAt);
    expect(result.updatedAt).toStrictEqual(client.updatedAt);
  });
});
