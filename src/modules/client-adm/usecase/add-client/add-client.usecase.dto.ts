export interface InputAddClientDto {
  id?: string;
  name: string;
  email: string;
  document: string;
  street: string;
  number: number;
  complement?: string;
  city: string;
  zipCode: string;
  state: string;
}

export interface OutputAddClientDto {
  id: string;
  name: string;
  email: string;
  document: string;
  street: string;
  number: number;
  complement?: string;
  city: string;
  zipCode: string;
  state: string;
  createdAt: Date;
  updatedAt: Date;
}
