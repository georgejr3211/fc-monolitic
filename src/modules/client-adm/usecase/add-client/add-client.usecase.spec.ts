import { AddClientUseCase } from "./add-client.usecase";

const MockRepository = () => ({
  add: jest.fn(),
  find: jest.fn(),
});

describe("Add Client UseCase Unit Test", () => {
  it("should add a client", async () => {
    const repository = MockRepository();
    const usecase = new AddClientUseCase(repository);
    const input = {
      name: "John Doe",
      email: "x@x.com",
      city: "New York",
      state: "NY",
      street: "Some Street",
      number: 1,
      complement: "",
      document: "0000",
      zipCode: "0000",
    };

    const result = await usecase.execute(input);

    expect(result).toEqual({
      id: expect.any(String),
      name: "John Doe",
      email: "x@x.com",
      createdAt: expect.any(Date),
      updatedAt: expect.any(Date),
      city: "New York",
      state: "NY",
      street: "Some Street",
      number: 1,
      complement: "",
      document: "0000",
      zipCode: "0000",
    });
  });
});
