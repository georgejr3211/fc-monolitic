import { Id } from "../../../@shared/domain/value-object/id.value-object";
import { UseCaseInterface } from "../../../@shared/usecase/usecase.interface";
import { Client } from "../../domain/client.entity";
import { ClientGateway } from "../../gateway/client.gateway";
import {
  InputAddClientDto,
  OutputAddClientDto,
} from "./add-client.usecase.dto";

export class AddClientUseCase implements UseCaseInterface {
  constructor(private clientRepository: ClientGateway) {}

  async execute(input: InputAddClientDto): Promise<OutputAddClientDto> {
    const client = new Client({
      id: new Id(input.id) || new Id(),
      name: input.name,
      email: input.email,
      city: input.city,
      complement: input.complement,
      document: input.document,
      number: input.number,
      state: input.state,
      street: input.street,
      zipCode: input.zipCode,
    });

    await this.clientRepository.add(client);
    
    const output = {
      id: client.id.value,
      name: client.name,
      email: client.email,
      city: client.city,
      complement: client.complement,
      document: client.document,
      number: client.number,
      state: client.state,
      street: client.street,
      zipCode: client.zipCode,
      createdAt: client.createdAt,
      updatedAt: client.updatedAt,
    };

    return output;
  }
}
