export interface InputFindClientDto {
  id: string;
}

export interface OutputFindClientDto {
  id: string;
  name: string;
  email: string;
  document: string;
  street: string;
  number: number;
  complement?: string;
  city: string;
  zipCode: string;
  state: string;
  createdAt: Date;
  updatedAt: Date;
}
