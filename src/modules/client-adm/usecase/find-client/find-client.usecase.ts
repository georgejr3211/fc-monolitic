import { UseCaseInterface } from "../../../@shared/usecase/usecase.interface";
import { ClientGateway } from "../../gateway/client.gateway";
import {
  InputFindClientDto,
  OutputFindClientDto,
} from "./find-client.usecase.dto";

export class FindClientUseCase implements UseCaseInterface {
  constructor(private clientRepository: ClientGateway) {}

  async execute(input: InputFindClientDto): Promise<OutputFindClientDto> {
    const client = await this.clientRepository.find(input.id);

    const output = {
      id: client.id.value,
      name: client.name,
      email: client.email,
      city: client.city,
      complement: client.complement,
      document: client.document,
      number: client.number,
      state: client.state,
      street: client.street,
      zipCode: client.zipCode,
      createdAt: client.createdAt,
      updatedAt: client.updatedAt,
    };

    return output;
  }
}
