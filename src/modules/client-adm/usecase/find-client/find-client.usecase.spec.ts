import { Id } from "../../../@shared/domain/value-object/id.value-object";
import { Client } from "../../domain/client.entity";
import { FindClientUseCase } from "./find-client.usecase";

const client = new Client({
  id: new Id("1"),
  name: "John Doe",
  email: "x@x.com",
  city: "New York",
  state: "NY",
  street: "Some Street",
  number: 1,
  complement: "",
  document: "0000",
  zipCode: "0000",
});

const MockRepository = () => ({
  add: jest.fn(),
  find: jest.fn().mockReturnValue(Promise.resolve(client)),
});

describe("Find Client UseCase Unit Test", () => {
  it("should find a client", async () => {
    const repository = MockRepository();
    const usecase = new FindClientUseCase(repository);
    const input = {
      id: "1",
    };

    const result = await usecase.execute(input);

    expect(result).toEqual({
      id: "1",
      name: "John Doe",
      email: "x@x.com",
      createdAt: expect.any(Date),
      updatedAt: expect.any(Date),
      city: "New York",
      state: "NY",
      street: "Some Street",
      number: 1,
      complement: "",
      document: "0000",
      zipCode: "0000",
    });
  });
});
