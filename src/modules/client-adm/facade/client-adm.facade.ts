import { UseCaseInterface } from "../../@shared/usecase/usecase.interface";
import {
  ClientAdmFacadeInterface,
  InputAddClientFacadeDto,
  InputFindClientFacadeDto,
  OutputFindClientFacadeDto,
} from "./client-adm.facade.interface";

export interface UseCaseProps {
  findUsecase: UseCaseInterface;
  addUsecase: UseCaseInterface;
}

export class ClientAdmFacade implements ClientAdmFacadeInterface {
  private _findUsecase: UseCaseInterface;
  private _addUsecase: UseCaseInterface;

  constructor(props: UseCaseProps) {
    this._findUsecase = props.findUsecase;
    this._addUsecase = props.addUsecase;
  }

  async add(input: InputAddClientFacadeDto): Promise<void> {
    await this._addUsecase.execute(input);
  }

  async find(
    input: InputFindClientFacadeDto
  ): Promise<OutputFindClientFacadeDto> {
    return await this._findUsecase.execute(input);
  }
}
