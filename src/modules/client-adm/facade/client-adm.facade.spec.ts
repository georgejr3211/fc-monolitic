import { Sequelize } from "sequelize-typescript";

import { ClientAdmFacadeFactory } from "../factory/client-adm.facade.factory";
import { ClientModel } from "../repository/client.model";
import { InputAddClientFacadeDto } from "./client-adm.facade.interface";

describe("Client Adm Facade Unit Test", () => {
  let sequelize: Sequelize;

  beforeEach(async () => {
    sequelize = new Sequelize({
      dialect: "sqlite",
      storage: ":memory:",
      logging: false,
    });

    sequelize.addModels([ClientModel]);

    await sequelize.sync();
  });

  afterEach(async () => {
    await sequelize.close();
  });

  it("should create a client", async () => {
    const facade = ClientAdmFacadeFactory.create();

    const input: InputAddClientFacadeDto = {
      id: "1",
      name: "John Doe",
      email: "x@x.com",
      document: "0000",
      city: "New York",
      state: "NY",
      street: "Some Street",
      number: 1,
      complement: "",
      zipCode: "0000",
    };

    await facade.add(input);

    const client = await ClientModel.findOne({ where: { id: input.id } });

    expect(client).toBeDefined();
    expect(client?.name).toBe(input.name);
    expect(client?.email).toBe(input.email);
    expect(client?.document).toBe(input.document);
    expect(client?.city).toBe(input.city);
    expect(client?.state).toBe(input.state);
    expect(client?.street).toBe(input.street);
    expect(client?.number).toBe(input.number);
    expect(client?.complement).toBe(input.complement);
    expect(client?.zipCode).toBe(input.zipCode);
  });

  it("should find a client", async () => {
    const facade = ClientAdmFacadeFactory.create();

    await facade.add({
      id: "1",
      name: "John Doe",
      email: "x@x.com",
      document: "0000",
      city: "New York",
      state: "NY",
      street: "Some Street",
      number: 1,
      complement: "",
      zipCode: "0000",
    });

    const client = await facade.find({ id: "1" });

    expect(client).toBeDefined();
    expect(client?.id).toBe("1");
    expect(client?.name).toBe("John Doe");
    expect(client?.email).toBe("x@x.com");
    expect(client?.document).toBe("0000");
    expect(client?.city).toBe("New York");
    expect(client?.state).toBe("NY");
    expect(client?.street).toBe("Some Street");
    expect(client?.number).toBe(1);
    expect(client?.complement).toBe("");
    expect(client?.zipCode).toBe("0000");
  });
});
