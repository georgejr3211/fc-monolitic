export interface InputAddClientFacadeDto {
  id?: string;
  name: string;
  email: string;
  document: string;
  street: string;
  number: number;
  complement?: string;
  city: string;
  zipCode: string;
  state: string;
}

export interface InputFindClientFacadeDto {
  id: string;
}

export interface OutputFindClientFacadeDto {
  id: string;
  name: string;
  email: string;
  document: string;
  street: string;
  number: number;
  complement?: string;
  city: string;
  state: string;
  zipCode: string;
  createdAt: Date;
  updatedAt: Date;
}

export interface ClientAdmFacadeInterface {
  add(input: InputAddClientFacadeDto): Promise<void>;
  find(input: InputFindClientFacadeDto): Promise<OutputFindClientFacadeDto>;
}
