import { Sequelize } from "sequelize-typescript";

import { Id } from "../../@shared/domain/value-object/id.value-object";
import { Transaction } from "../domain/transaction";
import { TransactionModel } from "./transaction.model";
import { TransactionRepository } from "./transaction.repository";

describe("Transaction Repository Test", () => {
  let sequelize: Sequelize;

  beforeEach(async () => {
    sequelize = new Sequelize({
      dialect: "sqlite",
      storage: ":memory:",
      logging: false,
    });

    sequelize.addModels([TransactionModel]);

    await sequelize.sync();
  });

  afterEach(async () => {
    await sequelize.close();
  });

  it("should save a transaction", async () => {
    const transaction = new Transaction({
      id: new Id("1"),
      amount: 100,
      orderId: "1",
      status: "approved",
    });

    const repository = new TransactionRepository();

    const savedTransaction = await repository.save(transaction);

    expect(transaction.id.value).toBe(savedTransaction.id.value);
    expect(transaction.amount).toBe(savedTransaction.amount);
    expect(transaction.orderId).toBe(savedTransaction.orderId);
    expect(transaction.status).toBe(savedTransaction.status);
  });
});
