import { Column, Model, PrimaryKey, Table } from "sequelize-typescript";

@Table({
  tableName: "transactions",
  timestamps: false,
})
export class TransactionModel extends Model {
  @PrimaryKey
  @Column({ allowNull: false })
  public id!: string;

  @Column({ allowNull: false })
  public amount!: number;

  @Column({ allowNull: false, field: "order_id" })
  public orderId!: string;

  @Column({ allowNull: false })
  public status!: string;

  @Column({ allowNull: false, field: "created_at" })
  public createdAt!: Date;

  @Column({ allowNull: false, field: "updated_at" })
  public updatedAt!: Date;
}
