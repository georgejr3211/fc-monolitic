import { PaymentFacade } from "../facade/payment.facade";
import { PaymentFacadeInterface } from "../facade/payment.facade.interface";
import { TransactionRepository } from "../repository/transaction.repository";
import { ProcessPaymentUseCase } from "../usecase/process-payment/process-payment.usecase";

export class PaymentFacadeFactory {
  static create(): PaymentFacadeInterface {
    const transactionRepository = new TransactionRepository();
    const processPaymentUsecase = new ProcessPaymentUseCase(
      transactionRepository
    );
    return new PaymentFacade(processPaymentUsecase);
  }
}
