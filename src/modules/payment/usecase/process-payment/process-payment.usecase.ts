import { UseCaseInterface } from "../../../@shared/usecase/usecase.interface";
import { Transaction } from "../../domain/transaction";
import { PaymentGateway } from "../../gateway/payment-gateway.interface";
import {
  InputProcessPaymentDto,
  OutputProcessPaymentDto,
} from "./process-payment.dto";

export class ProcessPaymentUseCase implements UseCaseInterface {
  constructor(private readonly paymentRepository: PaymentGateway) {
    this.paymentRepository = paymentRepository;
  }

  async execute(
    input: InputProcessPaymentDto
  ): Promise<OutputProcessPaymentDto> {
    const transaction = new Transaction({
      amount: input.amount,
      orderId: input.orderId,
    });

    transaction.process();

    const savedTransaction = await this.paymentRepository.save(transaction);

    return {
      transactionId: savedTransaction.id.value,
      orderId: savedTransaction.orderId,
      amount: savedTransaction.amount,
      status: savedTransaction.status,
      createdAt: savedTransaction.createdAt,
      updatedAt: savedTransaction.updatedAt,
    };
  }
}
