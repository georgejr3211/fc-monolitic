import { Id } from "../../../@shared/domain/value-object/id.value-object";
import { Transaction } from "../../domain/transaction";
import { ProcessPaymentUseCase } from "./process-payment.usecase";

const transaction = new Transaction({
  id: new Id("1"),
  amount: 100,
  orderId: "1",
  status: "approved",
});

const MockRepository = () => {
  return {
    save: jest.fn().mockReturnValue(Promise.resolve(transaction)),
  };
};

describe("Process Payment Usecase Unit Test", () => {
  it("should return a transaction with status approved", async () => {
    const paymentRepository = MockRepository();
    const usecase = new ProcessPaymentUseCase(paymentRepository);
    const input = {
      amount: 100,
      orderId: "1",
    };

    const result = await usecase.execute(input);

    expect(result).toEqual({
      transactionId: "1",
      orderId: "1",
      amount: 100,
      status: "approved",
      createdAt: transaction.createdAt,
      updatedAt: transaction.updatedAt,
    });
  });

  it("should return a transaction with status declined", async () => {
    const paymentRepository = MockRepository();

    const declinedTransaction = new Transaction({
      id: new Id("1"),
      amount: 50,
      orderId: "1",
      status: "declined",
    });

    paymentRepository.save.mockReturnValue(Promise.resolve(declinedTransaction));

    const usecase = new ProcessPaymentUseCase(paymentRepository);
    const input = {
      amount: 50,
      orderId: "1",
    };

    const result = await usecase.execute(input);

    expect(result).toEqual({
      transactionId: "1",
      orderId: "1",
      amount: 50,
      status: "declined",
      createdAt: declinedTransaction.createdAt,
      updatedAt: declinedTransaction.updatedAt,
    });
  });
});
