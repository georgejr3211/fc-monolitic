import { UseCaseInterface } from "./../../@shared/usecase/usecase.interface";
import {
  InputPaymentFacadeDto,
  OutputPaymentFacadeDto,
  PaymentFacadeInterface,
} from "./payment.facade.interface";

export class PaymentFacade implements PaymentFacadeInterface {
  constructor(private processPaymentUsecase: UseCaseInterface) {}

  async process(input: InputPaymentFacadeDto): Promise<OutputPaymentFacadeDto> {
    return await this.processPaymentUsecase.execute(input);
  }
}
