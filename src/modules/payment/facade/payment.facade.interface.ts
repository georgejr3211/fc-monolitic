export class InputPaymentFacadeDto {
  amount: number;
  orderId: string;
}

export class OutputPaymentFacadeDto {
  transactionId: string;
  orderId: string;
  amount: number;
  status: string;
  createdAt: Date;
  updatedAt: Date;
}

export interface PaymentFacadeInterface {
  process(input: InputPaymentFacadeDto): Promise<OutputPaymentFacadeDto>;
}
