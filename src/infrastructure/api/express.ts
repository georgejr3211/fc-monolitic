import express, { Express } from 'express';
import { Sequelize } from 'sequelize-typescript';

import { OrderModel } from '../../modules/checkout/repository/model/order.model';
import { OrderProductModel } from '../../modules/checkout/repository/model/order_products.model';
import { ClientModel } from '../../modules/client-adm/repository/client.model';
import { InvoiceModel } from '../../modules/invoice/repository/invoice.model';
import { TransactionModel } from '../../modules/payment/repository/transaction.model';
import { ProductModel } from '../../modules/product-adm/repository/product.model';
import { ClientModel as ClientCheckoutModel } from './../../modules/checkout/repository/model/client.model';
import { ProductModel as ProductCheckoutModel } from './../../modules/checkout/repository/model/product.model';
import { ProductModel as ProductInvoiceModel } from './../../modules/invoice/repository/product.model';
import { ProductModel as ProductCatalogModel } from './../../modules/store-catalog/repository/product.model';
import { checkoutRoute } from './routes/checkout.route';
import { clientRoute } from './routes/client.route';
import { invoiceRoute } from './routes/invoice.route';
import { productRoute } from './routes/product.route';

export const app: Express = express();
app.use(express.json());
app.use("/clients", clientRoute);
app.use("/products", productRoute);
app.use("/checkout", checkoutRoute);
app.use("/invoice", invoiceRoute);

export let sequelize: Sequelize;

async function setupDb() {
  sequelize = new Sequelize({
    dialect: "sqlite",
    storage: ":memory:",
    logging: false,
  });

  await sequelize.addModels([
    ProductModel,
    ProductCheckoutModel,
    ProductCatalogModel,
    ProductInvoiceModel,
    ClientModel,
    TransactionModel,
    ClientCheckoutModel,
    InvoiceModel,
    OrderProductModel,
    OrderModel
  ]);

  await sequelize.sync();
}

setupDb();
