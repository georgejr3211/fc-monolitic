import { app, sequelize } from "../express";
import request from "supertest";

describe("E2E test for client", () => {
  beforeEach(async () => {
    await sequelize.sync({ force: true });
  });

  afterAll(async () => {
    await sequelize.close();
  });

  it("should create a client", async () => {
    const response = await request(app).post("/clients").send({
      name: "John Doe",
      email: "john@anymail.com",
      document: "00000000000",
      street: "Any Street",
      number: 12,
      complement: "Any Complement",
      city: "Any City",
      zipCode: "00000000",
      state: "Any State",
    });

    expect(response.status).toBe(201);
  });
});
