import { app, sequelize } from "../express";
import request from "supertest";
import { InvoiceFacadeFactory } from "../../../modules/invoice/factory/invoice.facade.factory";

describe("E2E test for invoice", () => {
  beforeEach(async () => {
    await sequelize.sync({ force: true });
  });

  // afterAll(async () => {
  //   await sequelize.close();
  // });

  it("should get a invoice", async () => {
    const invoice = InvoiceFacadeFactory.create();
    const input = {
      id: '1',
      name: "John Doe",
      document: "12345678910",
      street: "street",
      city: "city",
      state: "state",
      zipCode: "zipCode",
      number: 1,
      complement: "complement",
      items: [
        {
          id: "1",
          name: "item 1",
          price: 10,
        },
        {
          id: "2",
          name: "item 2",
          price: 20,
        },
      ],
    };

    const invoiceResponse = await invoice.generate(input);

    const response = await request(app).get(`/invoice/${invoiceResponse.id}`).send();
    expect(response.status).toBe(200);
  });
});
