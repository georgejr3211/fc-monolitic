import { app, sequelize } from "../express";
import request from "supertest";

describe("E2E test for product", () => {
  beforeEach(async () => {
    await sequelize.sync({ force: true });
  });

  afterAll(async () => {
    await sequelize.close();
  });

  it("should create a product", async () => {
    const response = await request(app).post('/products').send({
        name: 'any name',
        description: 'any description',
        purchasePrice: 100,
        stock: 50,
    });

    expect(response.status).toBe(201);
    expect(response.body.name).toBe('any name');
    expect(response.body.purchasePrice).toBe(100);
    expect(response.body.stock).toBe(50);
  });
});
