import request from 'supertest';

import { app, sequelize } from '../express';

describe("E2E test for checkout", () => {
  beforeEach(async () => {
    await sequelize.sync({ force: true });
  });

  afterAll(async () => {
    await sequelize.close();
  });

  it("should create a checkout", async () => {
    const clientResponse = await request(app).post("/clients").send({
      id: "1",
      name: "John Doe",
      email: "john@anymail.com",
      document: "00000000000",
      street: "Any Street",
      number: 12,
      complement: "Any Complement",
      city: "Any City",
      zipCode: "00000000",
      state: "Any State",
    });

    const productResponse = await request(app).post("/products").send({
      id: "1",
      name: "any name",
      description: "any description",
      purchasePrice: 100,
      salesPrice: 100,
      stock: 50,
    });

    const response = await request(app)
      .post("/checkout")
      .send({
        clientId: "1",
        products: [
          {
            productId: productResponse.body.id,
          },
        ],
      });

    expect(response.status).toBe(201);
    expect(response.body).toEqual({
      id: expect.any(String),
      invoiceId: expect.any(String),
      status: "approved",
      total: 100,
      products: [{ productId: productResponse.body.id }],
    });
  });
});
