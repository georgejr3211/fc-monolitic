import { Request, Response, Router } from 'express';

import {
  CheckoutAdmFacadeInterface,
  InputCheckoutFacadeDto,
} from '../../../modules/checkout/facade/checkout.facade.interface';
import { CheckoutFacadeFactory } from '../../../modules/checkout/factory/checkout.facade.factory';

export const checkoutRoute = Router();

checkoutRoute.post("/", async (req: Request, res: Response) => {
  const checkoutAdmFacade: CheckoutAdmFacadeInterface =
    CheckoutFacadeFactory.create();

  try {
    const input: InputCheckoutFacadeDto = {
      clientId: req.body.clientId,
      products: req.body.products,
    };

    const output = await checkoutAdmFacade.checkout(input);
    res.status(201).json(output);
  } catch (error) {
    res.status(400).json(error);
  }
});
