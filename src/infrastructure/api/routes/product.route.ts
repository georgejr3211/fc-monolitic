import { Request, Response, Router } from "express";
import {
  InputAddProductFacadeDto,
  ProductAdmFacadeInterface,
} from "../../../modules/product-adm/facade/product-adm.facade.interface";
import { ProductAdmFacadeFactory } from "../../../modules/product-adm/factory/product-adm.facade.factory";

export const productRoute = Router();

productRoute.post("/", async (req: Request, res: Response) => {
  const productAdmFacade: ProductAdmFacadeInterface =
    ProductAdmFacadeFactory.create();

  try {
    const input: InputAddProductFacadeDto = {
      name: req.body.name,
      description: req.body.description,
      purchasePrice: req.body.purchasePrice,
      salesPrice: req.body.salesPrice,
      stock: req.body.stock,
    };

    const output = await productAdmFacade.addProduct(input);
    res.status(201).json(output);
  } catch (error) {
    res.status(400).json(error);
  }
});