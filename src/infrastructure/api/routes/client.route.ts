import { Request, Response, Router } from "express";
import { ClientAdmFacadeInterface } from "../../../modules/client-adm/facade/client-adm.facade.interface";
import { ClientAdmFacadeFactory } from "../../../modules/client-adm/factory/client-adm.facade.factory";
import { InputAddClientDto } from "../../../modules/client-adm/usecase/add-client/add-client.usecase.dto";

export const clientRoute = Router();

clientRoute.post("/", async (req: Request, res: Response) => {
  const clientAdmFacade: ClientAdmFacadeInterface =
    ClientAdmFacadeFactory.create();

  try {
    const input: InputAddClientDto = {
      id: req.body.id,
      name: req.body.name,
      email: req.body.email,
      document: req.body.document,
      street: req.body.street,
      number: req.body.number,
      complement: req.body.complement,
      city: req.body.city,
      zipCode: req.body.zipCode,
      state: req.body.state,
    };

    const output = await clientAdmFacade.add(input);
    res.status(201).json(output);
  } catch (error) {
    res.status(400).json(error);
  }
});
