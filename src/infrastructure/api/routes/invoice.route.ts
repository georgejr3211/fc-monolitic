import { Request, Response, Router } from "express";
import {
  InputFindInvoiceFacadeDto,
  InvoiceFacadeInterface,
} from "../../../modules/invoice/facade/invoice.facade.dto";
import { InvoiceFacadeFactory } from "../../../modules/invoice/factory/invoice.facade.factory";

export const invoiceRoute = Router();

invoiceRoute.get("/:id", async (req: Request, res: Response) => {
  const invoiceFacade: InvoiceFacadeInterface = InvoiceFacadeFactory.create();

  try {
    const input: InputFindInvoiceFacadeDto = {
      id: req.params.id,
    };

    const output = await invoiceFacade.find(input);
    res.status(200).json(output);
  } catch (error) {
    res.status(400).json(error);
  }
});
